# Shopping Cart
### E-Commerce Shopping Cart Implementation

#### Description
Shopping Cart is based on following Class: [ShoppingClass](src/main/java/com/taylan/shoppingcart/service/ShoppingCart.java)

#### Tech Stack
Project is used following technologies:
- Maven
- Spring Boot
- Spring Test
- Junit
- AssertJ
- JaCoCo

#### Architecture
Project is implemented by TDD. As always, before implementing project was designing up-front.
Here is general architecture:
![architecture-design][architecture]

[architecture]: src/main/resources/shopping_cart_system_design.png


#### Modules
There are two modules in project; Domain, Service

##### Domain
> Data representation layer of project. We have model and enumtype here for different type of domains.

##### Service
> Play Business Logic layer of project. In this layer we implement our business logic for application requirements.


#### Testing
You can run test for application from terminal. In order to do that, you need to install maven plugin to your local first: https://maven.apache.org/install.html

After, you can run following terminal command:
```bash
mvn clean install test
```
This command should compile your project then run every test against the application.

#### Reporting
If you want analyze test code coverage gently you can use JaCoCo Report in the application!

After you installed maven plugin in your local, run following command from your terminal: 
```bash
mvn jacoco:report
```
This command will generate "index.html" HTML file to you under directory at "/target/site/jacoco/"
Open "index.html" in you popular browser and you good to go, analyzing!

