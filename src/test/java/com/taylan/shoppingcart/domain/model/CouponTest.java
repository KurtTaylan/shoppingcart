package com.taylan.shoppingcart.domain.model;

import com.taylan.shoppingcart.domain.enumtype.DiscountApplicationType;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CouponTest {

    @Test
    public void should_coupon_have_minimum_price_threshold_when_coupon_is_created() {
        //when
        Coupon coupon = new Coupon(new BigDecimal(50.0), new BigDecimal(20), DiscountApplicationType.RATE);

        //then
        assertThat(coupon.getDiscountApplicationType().getValue()).isEqualTo(DiscountApplicationType.RATE.getValue());
        assertThat(coupon.getDiscountQuantity()).isEqualTo(new BigDecimal(20));
        assertThat(coupon.getMinimumPriceThreshold()).isEqualTo(new BigDecimal(50.0));
    }

    @Test
    public void should_coupon_is_applicable_when_shopping_cart_amount_is_greater_than_minimum_price_threshold() {
        //given
        Coupon coupon = new Coupon(new BigDecimal(50.0), new BigDecimal(20), DiscountApplicationType.RATE);

        //when
        boolean applicableForAmount = coupon.isApplicableForAmount(BigDecimal.valueOf(500.0));

        //then
        assertThat(applicableForAmount).isTrue();
    }

    @Test
    public void should_coupon_is_applicable_when_shopping_cart_amount_is_equal_with_minimum_price_threshold() {
        //given
        Coupon coupon = new Coupon(new BigDecimal(50.0), new BigDecimal(20), DiscountApplicationType.RATE);

        //when
        boolean applicableForAmount = coupon.isApplicableForAmount(BigDecimal.valueOf(50.0));

        //then
        assertThat(applicableForAmount).isTrue();
    }

    @Test
    public void should_coupon_is_not_be_applicable_when_shopping_cart_amount_is_lesser_than_minimum_price_threshold() {
        //given
        Coupon coupon = new Coupon(new BigDecimal(50.0), new BigDecimal(20), DiscountApplicationType.RATE);

        //when
        boolean applicableForAmount = coupon.isApplicableForAmount(BigDecimal.valueOf(1.0));

        //then
        assertThat(applicableForAmount).isFalse();
    }
}
