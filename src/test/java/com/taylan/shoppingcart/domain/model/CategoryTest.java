package com.taylan.shoppingcart.domain.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Model Rules test for Category Domain
 */
public class CategoryTest {

    @Test
    public void should_category_has_title() {
        //when
        Category category = new Category("category");

        //then
        assertThat(category.getTitle()).isEqualTo("category");
    }

    @Test
    public void should_category_have_parent_category_when_parent_category_is_defined() {
        //given
        Category cosmeticCategory = new Category("cosmetic");

        //when
        Category shampooCategory = new Category("shampoo", cosmeticCategory);

        //then
        assertThat(shampooCategory.getParentCategory()).isNotNull();

        Category parentCategory = shampooCategory.getParentCategory();
        assertThat(parentCategory.getTitle()).isEqualTo("cosmetic");
        assertThat(parentCategory.getParentCategory()).isNull();
    }

    @Test
    public void should_two_category_equals_when_they_have_same_title() {
        //given
        Category categoryOne = new Category("Tech");
        Category categoryTwo = new Category("Tech");

        //when
        boolean equals = categoryOne.equals(categoryTwo);

        //then
        assertThat(equals).isTrue();
    }
}
