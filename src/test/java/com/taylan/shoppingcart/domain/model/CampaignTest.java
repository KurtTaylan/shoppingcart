package com.taylan.shoppingcart.domain.model;

import com.taylan.shoppingcart.domain.enumtype.DiscountApplicationType;
import com.taylan.shoppingcart.domain.model.Campaign;
import com.taylan.shoppingcart.domain.model.Category;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class CampaignTest {

    @Test
    public void should_campaign_with_discount_application_type_RATE_has_category_when_campaign_discount_rule_is_created() {
        //given
        Category manCategory = new Category("MAN");

        //when
        Campaign campaign = new Campaign(manCategory, new BigDecimal(50.0), 3, DiscountApplicationType.RATE);

        //then
        assertThat(campaign.getCategory()).isNotNull();
        assertThat(campaign.getCategory().getTitle()).isEqualTo("MAN");
        assertThat(campaign.getDiscountQuantity()).isEqualTo(new BigDecimal(50.0));
        assertThat(campaign.getProductThreshold()).isEqualTo(3);
        assertThat(campaign.getDiscountApplicationType()).isEqualTo(DiscountApplicationType.RATE);
    }

    @Test
    public void should_campaign_with_discount_application_type_AMOUNT_has_category_when_campaign_discount_rule_is_created() {
        //given
        Category manCategory = new Category("MAN");

        //when
        Campaign campaign = new Campaign(manCategory, new BigDecimal(50.0), 3, DiscountApplicationType.AMOUNT);

        //then
        assertThat(campaign.getCategory()).isNotNull();
        assertThat(campaign.getCategory().getTitle()).isEqualTo("MAN");
        assertThat(campaign.getDiscountQuantity()).isEqualTo(new BigDecimal(50.0));
        assertThat(campaign.getProductThreshold()).isEqualTo(3);
        assertThat(campaign.getDiscountApplicationType().getValue()).isEqualTo(DiscountApplicationType.AMOUNT.getValue());
    }
}
