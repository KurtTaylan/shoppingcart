package com.taylan.shoppingcart.domain.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Model Rules test for Product Domain
 */
public class ProductTest {

    @Test
    public void should_product_has_title_and_price() {
        //when
        Product product = new Product("Apple", new BigDecimal(10), new Category("UNKNOWN"));

        //then
        assertThat(product.getTitle()).isEqualTo("Apple");
        assertThat(product.getPrice()).isEqualTo(new BigDecimal(10));
    }

    @Test
    public void should_product_belong_to_a_category() {
        //given
        Category techCategory = new Category("Tech");

        //when
        Product product = new Product("I-watch", new BigDecimal(1512), techCategory);

        //then
        Category category = product.getCategory();
        assertThat(category).isNotNull();
        assertThat(category.getTitle()).isEqualTo("Tech");
    }

    @Test
    public void should_two_product_equals_when_they_have_same_title_and_price_and_category() {
        //given
        Category techCategory = new Category("Tech");

        Product productOne = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product productTwo = new Product("I-watch", new BigDecimal(1512), techCategory);

        //when
        boolean equals = productOne.equals(productTwo);

        //then
        assertThat(equals).isTrue();
    }
}