package com.taylan.shoppingcart.service;

import com.taylan.shoppingcart.domain.enumtype.DiscountApplicationType;
import com.taylan.shoppingcart.domain.model.Campaign;
import com.taylan.shoppingcart.domain.model.Category;
import com.taylan.shoppingcart.domain.model.Coupon;
import com.taylan.shoppingcart.domain.model.Product;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ShoppingCartTest {

    @Test
    public void should_shopping_cart_has_product_map() {
        //when
        ShoppingCart shoppingCart = new ShoppingCart();

        //then
        assertThat(shoppingCart.getProductQuantityMap()).isNotNull();
        assertThat(shoppingCart.getProductQuantityMap().size()).isEqualTo(0);
    }

    @Test
    public void should_add_product_to_shopping_cart_with_quantity() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        //when
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 3);

        //then
        assertThat(shoppingCart.getProductQuantityMap()).isNotNull();

        Map<Product, Integer> productQuantityMap = shoppingCart.getProductQuantityMap();
        assertThat(productQuantityMap.size()).isEqualTo(2);
        assertThat(productQuantityMap.containsKey(iWatchProduct)).isTrue();
        assertThat(productQuantityMap.containsKey(macBookProduct)).isTrue();
        assertThat(productQuantityMap.get(iWatchProduct)).isEqualTo(2);
        assertThat(productQuantityMap.get(macBookProduct)).isEqualTo(3);
    }

    @Test
    public void should_shopping_cart_apply_campaign_discount_by_product_category() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Campaign twentyPercentDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20.0), 3, DiscountApplicationType.RATE);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);

        //when
        shoppingCart.applyCampaignDiscount(twentyPercentDiscountAfterThreeProduct);

        //then
        BigDecimal campaignDiscountAmount = shoppingCart.getCampaignDiscountAmount();
        assertThat(campaignDiscountAmount).isEqualTo(BigDecimal.valueOf(16976));
    }

    @Test
    public void should_shopping_cart_apply_twenty_thousand_turkish_lira_campaign_when_maximum_campaign_discount_by_product_category_is_that() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Campaign fifteenPercentDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(15.0), 3, DiscountApplicationType.RATE);
        Campaign twentyPercentDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20.0), 3, DiscountApplicationType.RATE);
        Campaign twentyThousandTurkishLiraDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20000.0), 3, DiscountApplicationType.AMOUNT);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);

        //when
        shoppingCart.applyCampaignDiscount(fifteenPercentDiscountAfterThreeProduct, twentyPercentDiscountAfterThreeProduct, twentyThousandTurkishLiraDiscountAfterThreeProduct);

        //then
        BigDecimal campaignDiscountAmount = shoppingCart.getCampaignDiscountAmount();
        assertThat(campaignDiscountAmount).isEqualTo(new BigDecimal(20000));
    }

    @Test
    public void should_shopping_cart_not_apply_any_campaign_discount_when_campaign_discount_application_type_is_not_applicable() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Campaign twentyThousandTurkishLiraDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20000.0), 3, null);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);

        //when
        shoppingCart.applyCampaignDiscount(twentyThousandTurkishLiraDiscountAfterThreeProduct);

        //then
        BigDecimal campaignDiscountAmount = shoppingCart.getCampaignDiscountAmount();
        assertThat(campaignDiscountAmount).isEqualTo(new BigDecimal(0));
    }

    @Test
    public void should_shopping_cart_apply_coupon_discount_by_AMOUNT_when_shopping_cart_purchase_amount_is_greater_than_coupon_price_threshold() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Coupon fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand = new Coupon(new BigDecimal(10000), new BigDecimal(500), DiscountApplicationType.AMOUNT);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);

        //when
        shoppingCart.applyCouponDiscount(fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand);

        //then
        BigDecimal couponDiscountAmount = shoppingCart.getCouponDiscountAmount();
        assertThat(couponDiscountAmount).isEqualTo(new BigDecimal(500));
    }

    @Test
    public void should_shopping_cart_apply_coupon_discount_by_RATE_when_shopping_cart_purchase_amount_is_greater_than_coupon_price_threshold() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Coupon fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand = new Coupon(new BigDecimal(50000), new BigDecimal(20), DiscountApplicationType.RATE);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);

        //when
        shoppingCart.applyCouponDiscount(fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand);

        //then
        BigDecimal couponDiscountAmount = shoppingCart.getCouponDiscountAmount();
        assertThat(couponDiscountAmount).isEqualTo(new BigDecimal(17581));
    }

    @Test
    public void should_shopping_cart_apply_coupon_discount_by_AMOUNT_when_shopping_cart_applied_campaign_discount() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Campaign twentyThousandTurkishLiraDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20000.0), 3, DiscountApplicationType.AMOUNT);
        Coupon fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand = new Coupon(new BigDecimal(10000), new BigDecimal(500), DiscountApplicationType.AMOUNT);


        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);
        shoppingCart.applyCampaignDiscount(twentyThousandTurkishLiraDiscountAfterThreeProduct);

        //when
        shoppingCart.applyCouponDiscount(fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand);

        //then
        BigDecimal couponDiscountAmount = shoppingCart.getCouponDiscountAmount();
        assertThat(couponDiscountAmount).isEqualTo(new BigDecimal(500));
    }

    @Test
    public void should_shopping_cart_apply_coupon_discount_by_RATE_when_shopping_cart_applied_campaign_discount() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Campaign twentyThousandTurkishLiraDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20000.0), 3, DiscountApplicationType.AMOUNT);
        Coupon twentyPercentDiscountCouponForCartPriceMoreThanFiftyThousand = new Coupon(new BigDecimal(50000), new BigDecimal(20), DiscountApplicationType.RATE);


        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);
        shoppingCart.applyCampaignDiscount(twentyThousandTurkishLiraDiscountAfterThreeProduct);

        //when
        shoppingCart.applyCouponDiscount(twentyPercentDiscountCouponForCartPriceMoreThanFiftyThousand);

        //then
        BigDecimal couponDiscountAmount = shoppingCart.getCouponDiscountAmount();
        assertThat(couponDiscountAmount).isEqualTo(new BigDecimal(13581));
    }

    @Test
    public void should_shopping_cart_not_apply_coupon_discount_when_shopping_cart_applied_campaign_discount_and_subtracted_price_is_lesser_than_coupon_threshold() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        Campaign twentyThousandTurkishLiraDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(80000.0), 3, DiscountApplicationType.AMOUNT);
        Coupon fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand = new Coupon(new BigDecimal(10000), new BigDecimal(10000), DiscountApplicationType.AMOUNT);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 4);
        shoppingCart.applyCampaignDiscount(twentyThousandTurkishLiraDiscountAfterThreeProduct);

        //when
        shoppingCart.applyCouponDiscount(fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand);

        //then
        BigDecimal couponDiscountAmount = shoppingCart.getCouponDiscountAmount();
        assertThat(couponDiscountAmount).isEqualTo(new BigDecimal(0));
    }

    @Test
    public void should_shopping_cart_print_shopping_cart_details() {
        //given
        Category manCategory = new Category("Man");
        Product socksProduct = new Product("Man Socks", new BigDecimal(15), manCategory);

        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 3);
        shoppingCart.addProduct(socksProduct, 3);

        Campaign fifteenPercentDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(15.0), 3, DiscountApplicationType.RATE);
        Campaign twentyPercentDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20.0), 3, DiscountApplicationType.RATE);
        Campaign twentyThousandTurkishLiraDiscountAfterThreeProduct = new Campaign(techCategory, new BigDecimal(20000.0), 3, DiscountApplicationType.AMOUNT);
        Coupon fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand = new Coupon(new BigDecimal(10000), new BigDecimal(500), DiscountApplicationType.AMOUNT);

        shoppingCart.applyCampaignDiscount(fifteenPercentDiscountAfterThreeProduct, twentyPercentDiscountAfterThreeProduct, twentyThousandTurkishLiraDiscountAfterThreeProduct);
        shoppingCart.applyCouponDiscount(fiveHundredTurkishLiraCouponForCartPriceMoreThanTenThousand);

        //when
        String shoppingCardDetails = shoppingCart.print();

        //then
        assertThat(shoppingCardDetails).containsSequence("Welcome Shopping Retail:")
                                       .containsSequence("CategoryName: Tech, ProductName: Macbook, Quantity: 3, UnitPrice: 21220")
                                       .containsSequence("CategoryName: Tech, ProductName: I-watch, Quantity: 2, UnitPrice: 1512")
                                       .containsSequence("CategoryName: Man, ProductName: Man Socks, Quantity: 3, UnitPrice: 15")
                                       .containsSequence("TotalPrice: 66729, TotalDiscount(Campaign, Coupon): 20000, 500")
                                       .containsSequence("TotalAmount: 46229, DeliveryCost: 21.99");
    }
}
