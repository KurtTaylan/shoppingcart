package com.taylan.shoppingcart.service;

import com.taylan.shoppingcart.domain.model.Category;
import com.taylan.shoppingcart.domain.model.Product;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.assertj.core.api.Assertions.assertThat;

public class DeliveryTest {

    @Test
    public void should_delivery_has_costPerDelivery_and_costPerProduct_and_fixedCost_when_it_is_created() {
        //given
        BigDecimal costPerDelivery = new BigDecimal(5);
        BigDecimal costPerProduct = new BigDecimal(3);
        BigDecimal fixedCost = new BigDecimal(2);

        //when
        Delivery delivery = new Delivery(costPerDelivery, costPerProduct, fixedCost);

        //then
        assertThat(delivery.getCostPerDelivery()).isEqualTo(new BigDecimal(5));
        assertThat(delivery.getCostPerProduct()).isEqualTo(new BigDecimal(3));
        assertThat(delivery.getFixedCost()).isEqualTo(new BigDecimal(2));
    }

    @Test
    public void should_delivery_has_costPerDelivery_and_costPerProduct_and_fixedCost_with_2_99_when_it_is_created_without_fixed_cost_specified() {
        //given
        BigDecimal costPerDelivery = new BigDecimal(5);
        BigDecimal costPerProduct = new BigDecimal(3);

        //when
        Delivery delivery = new Delivery(costPerDelivery, costPerProduct);

        //then
        assertThat(delivery.getCostPerDelivery()).isEqualTo(new BigDecimal(5));
        assertThat(delivery.getCostPerProduct()).isEqualTo(new BigDecimal(3));
        assertThat(delivery.getFixedCost()).isEqualTo(new BigDecimal(2.99));
    }

    @Test
    public void should_delivery_calculate_delivery_cost_for_shopping_cart() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 3);

        BigDecimal costPerDelivery = new BigDecimal(5);
        BigDecimal costPerProduct = new BigDecimal(3);
        BigDecimal fixedCost = new BigDecimal(2);

        Delivery delivery = new Delivery(costPerDelivery, costPerProduct, fixedCost);

        //when
        BigDecimal deliveryCost = delivery.calculateDeliveryCostFor(shoppingCart);

        //then
        assertThat(deliveryCost).isEqualTo(new BigDecimal(13.00).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void should_delivery_calculate_delivery_cost_for_shopping_cart_when_fixed_cost_not_specified() {
        //given
        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 3);

        BigDecimal costPerDelivery = new BigDecimal(5);
        BigDecimal costPerProduct = new BigDecimal(3);

        Delivery delivery = new Delivery(costPerDelivery, costPerProduct);

        //when
        BigDecimal deliveryCost = delivery.calculateDeliveryCostFor(shoppingCart);

        //then
        assertThat(deliveryCost).isEqualTo(new BigDecimal(13.99).setScale(2, RoundingMode.HALF_EVEN));
    }

    @Test
    public void should_delivery_calculate_delivery_cost_for_shopping_cart_when_multi_categories_are_specified() {
        //given
        ShoppingCart shoppingCart = new ShoppingCart();
        Category manCategory = new Category("Man");
        Product socksProduct = new Product("Man Socks", new BigDecimal(15), manCategory);

        Category techCategory = new Category("Tech");
        Product iWatchProduct = new Product("I-watch", new BigDecimal(1512), techCategory);
        Product macBookProduct = new Product("Macbook", new BigDecimal(21220), techCategory);

        shoppingCart.addProduct(iWatchProduct, 2);
        shoppingCart.addProduct(macBookProduct, 3);
        shoppingCart.addProduct(socksProduct, 3);

        BigDecimal costPerDelivery = new BigDecimal(5);
        BigDecimal costPerProduct = new BigDecimal(3);
        BigDecimal fixedCost = new BigDecimal(10);

        Delivery delivery = new Delivery(costPerDelivery, costPerProduct, fixedCost);

        //when
        BigDecimal deliveryCost = delivery.calculateDeliveryCostFor(shoppingCart);

        //then
        assertThat(deliveryCost).isEqualTo(new BigDecimal(29.00).setScale(2, RoundingMode.HALF_EVEN));
    }
}
