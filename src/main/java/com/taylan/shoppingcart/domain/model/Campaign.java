package com.taylan.shoppingcart.domain.model;

import com.taylan.shoppingcart.domain.enumtype.DiscountApplicationType;

import java.math.BigDecimal;

public class Campaign {

    private Category category;
    private BigDecimal discountQuantity;
    private Integer productThreshold;
    private DiscountApplicationType discountApplicationType;

    public Campaign(Category category,
                    BigDecimal discountQuantity,
                    Integer productThreshold,
                    DiscountApplicationType discountApplicationType) {
        this.category = category;
        this.discountQuantity = discountQuantity;
        this.productThreshold = productThreshold;
        this.discountApplicationType = discountApplicationType;
    }

    public Category getCategory() {
        return category;
    }

    public BigDecimal getDiscountQuantity() {
        return discountQuantity;
    }

    public Integer getProductThreshold() {
        return productThreshold;
    }

    public DiscountApplicationType getDiscountApplicationType() {
        return discountApplicationType;
    }
}
