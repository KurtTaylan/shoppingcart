package com.taylan.shoppingcart.domain.model;

import com.taylan.shoppingcart.domain.enumtype.DiscountApplicationType;

import java.math.BigDecimal;

public class Coupon {

    private DiscountApplicationType discountApplicationType;
    private BigDecimal discountQuantity;
    private BigDecimal minimumPriceThreshold;

    public Coupon(BigDecimal minimumPriceThreshold,
                  BigDecimal discountQuantity,
                  DiscountApplicationType discountApplicationType) {
        this.minimumPriceThreshold = minimumPriceThreshold;
        this.discountQuantity = discountQuantity;
        this.discountApplicationType = discountApplicationType;
    }

    public DiscountApplicationType getDiscountApplicationType() {
        return discountApplicationType;
    }

    public BigDecimal getDiscountQuantity() {
        return discountQuantity;
    }

    public BigDecimal getMinimumPriceThreshold() {
        return minimumPriceThreshold;
    }

    public boolean isApplicableForAmount(BigDecimal amount) {
        return this.minimumPriceThreshold.compareTo(amount) <= 0;
    }
}
