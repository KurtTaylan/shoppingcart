package com.taylan.shoppingcart.domain.enumtype;

public enum DiscountApplicationType {
    RATE(1),
    AMOUNT(2);

    private Integer value;

    DiscountApplicationType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
