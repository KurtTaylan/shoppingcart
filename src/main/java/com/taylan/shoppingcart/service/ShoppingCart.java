package com.taylan.shoppingcart.service;

import com.taylan.shoppingcart.domain.model.Campaign;
import com.taylan.shoppingcart.domain.model.Category;
import com.taylan.shoppingcart.domain.model.Coupon;
import com.taylan.shoppingcart.domain.model.Product;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ShoppingCart {

    private Map<Product, Integer> productQuantityMap = new HashMap<>();
    private BigDecimal campaignDiscountAmount = BigDecimal.ZERO;
    private BigDecimal couponDiscountAmount = BigDecimal.ZERO;
    private BigDecimal totalShoppingCartAmount = BigDecimal.ZERO;

    public Map<Product, Integer> getProductQuantityMap() {
        return new HashMap<>(productQuantityMap);
    }

    public BigDecimal getCampaignDiscountAmount() {
        return this.campaignDiscountAmount;
    }

    public BigDecimal getCouponDiscountAmount() {
        return this.couponDiscountAmount;
    }

    public BigDecimal getTotalShoppingCartAmount() {
        return totalShoppingCartAmount;
    }

    public BigDecimal getTotalAmountAfterDiscounts() {
        return this.totalShoppingCartAmount
                .subtract(this.campaignDiscountAmount, MathContext.DECIMAL32)
                .subtract(this.couponDiscountAmount, MathContext.DECIMAL32);
    }

    public void addProduct(Product product, Integer quantity) {
        productQuantityMap.put(product, quantity);
        increaseTotalAmount(product.getPrice(), quantity);
    }

    public void applyCouponDiscount(Coupon coupon) {
        if (coupon.isApplicableForAmount(getRemainedAmountAfterCampaignDiscount())) {
            BigDecimal couponDiscountAmount = calculateCouponDiscountAmountForShoppingCart(coupon);
            increaseCouponDiscountAmount(couponDiscountAmount);
        }
    }

    public void applyCampaignDiscount(Campaign... campaignDiscountList) {
        BigDecimal maximumCampaignDiscountAmount = BigDecimal.ZERO;
        Map<Category, List<Product>> productListGroupByCategory = retrieveGroupedProductQuantityMapByCategory();
        List<Campaign> applicableCampaignList = retrieveApplicableCampaignList(productListGroupByCategory, campaignDiscountList);

        for (Campaign campaign : applicableCampaignList) {
            List<Product> categoryProductList = productListGroupByCategory.get(campaign.getCategory());
            BigDecimal categoryProductCampaignDiscountAmount = calculateCategoryCampaignDiscountAmount(campaign, categoryProductList);
            if (isGreaterThan(categoryProductCampaignDiscountAmount, maximumCampaignDiscountAmount))
                maximumCampaignDiscountAmount = categoryProductCampaignDiscountAmount;
        }

        if (isGreaterThan(maximumCampaignDiscountAmount, BigDecimal.ZERO))
            increaseCampaignDiscountAmount(maximumCampaignDiscountAmount);
    }

    public String print() {
        StringBuffer cartDetailBuffer = new StringBuffer();
        cartDetailBuffer.append("Welcome Shopping Retail:").append("\n");

        printProductListDetails(cartDetailBuffer);
        printPriceBeforeDiscount(cartDetailBuffer);
        printDiscounts(cartDetailBuffer);
        printPriceAfterDiscount(cartDetailBuffer);
        printDeliveryCost(cartDetailBuffer);
        return cartDetailBuffer.toString();
    }

    private void increaseTotalAmount(BigDecimal price, Integer quantity) {
        this.totalShoppingCartAmount = this.totalShoppingCartAmount.add(price.multiply(BigDecimal.valueOf(quantity)));
    }

    private BigDecimal calculateCouponDiscountAmountForShoppingCart(Coupon coupon) {
        BigDecimal couponDiscountAmount = BigDecimal.ZERO;
        switch (coupon.getDiscountApplicationType()) {
            case AMOUNT:
                couponDiscountAmount = coupon.getDiscountQuantity();
                break;
            case RATE:
                couponDiscountAmount = calculateCouponDiscountAmountForShoppingCartByRATE(coupon);
                break;
        }
        return couponDiscountAmount;
    }

    private void increaseCouponDiscountAmount(BigDecimal price) {
        this.couponDiscountAmount = this.couponDiscountAmount.add(price);
    }

    private BigDecimal getRemainedAmountAfterCampaignDiscount() {
        return this.totalShoppingCartAmount.subtract(this.campaignDiscountAmount);
    }

    private BigDecimal calculateCouponDiscountAmountForShoppingCartByRATE(Coupon coupon) {
        BigDecimal remainedAmountAfterCampaignDiscountIsApplied = getRemainedAmountAfterCampaignDiscount();
        return dividePriceByDiscountQuantity(remainedAmountAfterCampaignDiscountIsApplied, coupon.getDiscountQuantity());
    }

    public Map<Category, List<Product>> retrieveGroupedProductQuantityMapByCategory() {
        return getProductQuantityMap().keySet().stream().collect(Collectors.groupingBy(Product::getCategory));
    }

    private void printProductListDetails(StringBuffer cartDetailBuffer) {
        Map<Product, Integer> productQuantityMapCopy = getProductQuantityMap();
        Map<Category, List<Product>> categoryProductListMap = retrieveGroupedProductQuantityMapByCategory();
        categoryProductListMap.forEach((category, productList) -> {
            productList.forEach(product -> {
                cartDetailBuffer.append("CategoryName: ").append(category.getTitle());
                cartDetailBuffer.append(", ProductName: ").append(product.getTitle());
                cartDetailBuffer.append(", Quantity: ").append(productQuantityMapCopy.get(product));
                cartDetailBuffer.append(", UnitPrice: ").append(product.getPrice()).append("\n");
            });
        });
    }

    private void printPriceBeforeDiscount(StringBuffer cartDetailBuffer) {
        cartDetailBuffer.append("TotalPrice: ").append(getTotalShoppingCartAmount());
    }

    private void printDiscounts(StringBuffer cartDetailBuffer) {
        cartDetailBuffer
                .append(", TotalDiscount(Campaign, Coupon): ")
                .append(getCampaignDiscountAmount()).append(", ")
                .append(getCouponDiscountAmount())
                .append("\n");
    }

    private void printPriceAfterDiscount(StringBuffer cartDetailBuffer) {
        cartDetailBuffer.append("TotalAmount: ").append(getTotalAmountAfterDiscounts());
    }

    private void printDeliveryCost(StringBuffer cartDetailBuffer) {
        BigDecimal costPerDelivery = new BigDecimal(5);
        BigDecimal costPerProduct = new BigDecimal(3);
        Delivery delivery = new Delivery(costPerDelivery, costPerProduct);
        BigDecimal deliveryCost = delivery.calculateDeliveryCostFor(this);
        cartDetailBuffer.append(", DeliveryCost: ").append(deliveryCost);
    }

    private List<Campaign> retrieveApplicableCampaignList(Map<Category, List<Product>> productListGroupByCategory, Campaign[] campaignDiscountList) {
        return Arrays.stream(campaignDiscountList)
                .filter(campaign -> campaign.getDiscountApplicationType() != null)
                .filter(campaign -> productListGroupByCategory.containsKey(campaign.getCategory())).collect(Collectors.toList());
    }

    private BigDecimal calculateCategoryCampaignDiscountAmount(Campaign campaign, List<Product> categoryProductList) {
        return new BigDecimal(categoryProductList.stream()
                .filter(product -> isEqualsOrGreaterThan(BigDecimal.valueOf(getProductQuantityMap().get(product)), BigDecimal.valueOf(campaign.getProductThreshold())))
                .mapToDouble(product -> calculateCampaignDiscountAmountForShoppingCartProduct(campaign, product).doubleValue())
                .sum(), MathContext.DECIMAL32);
    }

    private void increaseCampaignDiscountAmount(BigDecimal discountPrice) {
        this.campaignDiscountAmount = this.campaignDiscountAmount.add(discountPrice);
    }

    private BigDecimal calculateCampaignDiscountAmountForShoppingCartProduct(Campaign campaign, Product product) {
        BigDecimal discountAmount = BigDecimal.ZERO;
        switch (campaign.getDiscountApplicationType()) {
            case RATE:
                discountAmount = calculateShoppingCartCampaignDiscountAmountForProductByRATE(campaign.getDiscountQuantity(), product);
                break;
            case AMOUNT:
                discountAmount = campaign.getDiscountQuantity();
                break;
        }

        return discountAmount;
    }

    private BigDecimal calculateShoppingCartCampaignDiscountAmountForProductByRATE(BigDecimal campaignQuantity, Product product) {
        Integer productQuantity = getProductQuantityMap().get(product);
        BigDecimal productUnitDiscount = dividePriceByDiscountQuantity(product.getPrice(), campaignQuantity);
        return productUnitDiscount.multiply(BigDecimal.valueOf(productQuantity));
    }

    private BigDecimal dividePriceByDiscountQuantity(BigDecimal price, BigDecimal discountQuantity) {
        return price.multiply(discountQuantity, MathContext.DECIMAL32).divide(new BigDecimal(100), RoundingMode.HALF_DOWN);
    }

    private boolean isGreaterThan(BigDecimal bigDecimalOne, BigDecimal bigDecimalTwo) {
        return bigDecimalOne.compareTo(bigDecimalTwo) > 0;
    }

    private boolean isEqualsOrGreaterThan(BigDecimal bigDecimalOne, BigDecimal bigDecimalTwo) {
        return bigDecimalOne.compareTo(bigDecimalTwo) >= 0;
    }
}
