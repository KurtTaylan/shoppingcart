package com.taylan.shoppingcart.service;

import com.taylan.shoppingcart.domain.model.Category;
import com.taylan.shoppingcart.domain.model.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class Delivery {

    private BigDecimal costPerDelivery;
    private BigDecimal costPerProduct;
    private BigDecimal fixedCost;

    public Delivery(BigDecimal costPerDelivery, BigDecimal costPerProduct, BigDecimal fixedCost) {
        this.costPerDelivery = costPerDelivery;
        this.costPerProduct = costPerProduct;
        this.fixedCost = fixedCost;
    }

    public Delivery(BigDecimal costPerDelivery, BigDecimal costPerProduct) {
        this(costPerDelivery, costPerProduct, new BigDecimal(2.99));
    }

    public BigDecimal getCostPerDelivery() {
        return costPerDelivery;
    }

    public BigDecimal getCostPerProduct() {
        return costPerProduct;
    }

    public BigDecimal getFixedCost() {
        return fixedCost;
    }

    public BigDecimal calculateDeliveryCostFor(ShoppingCart shoppingCart) {
        Map<Category, List<Product>> productQuantityMapByCategory = shoppingCart.retrieveGroupedProductQuantityMapByCategory();
        BigDecimal numberOfDeliveries = BigDecimal.valueOf(productQuantityMapByCategory.keySet().size());
        BigDecimal numberOfProducts = BigDecimal.valueOf(productQuantityMapByCategory.keySet().stream()
                .mapToInt(category -> productQuantityMapByCategory.get(category).size())
                .sum());

        BigDecimal totalCostDelivery = this.costPerDelivery.multiply(numberOfDeliveries);
        BigDecimal totalCostProduct = this.costPerProduct.multiply(numberOfProducts);

        BigDecimal totalDeliveryCost = totalCostDelivery.add(totalCostProduct).add(this.fixedCost);
        return totalDeliveryCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }
}
